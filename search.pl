#!/usr/bin/perl
use strict;
use warnings;
use Text::CSV;

my @rows;
my $csv = Text::CSV->new ( { binary => 1 } )
  or die "Cannot use CSV: ".Text::CSV->error_diag ();
open my $fh, "<:encoding(utf8)", "quenya.csv" or die "quenya.csv: $!";
while ( my $row = $csv->getline( $fh ) ) {
  push @rows, $row;
}
$csv->eof or $csv->error_diag();
close $fh;

$csv->eol("\n");

my @cols = ("stem", "defn", "infinitive", "present", "aorist", "future", "past", "perfect", "imperative", "active part.", "passive part.", "gerund");

sub search {
  my $term = shift;
  my @results = ();
  foreach my $row (@rows) {
	if (index($row->[1], $term) != -1) {
	  my $resstr = "";
	  foreach my $i (0..11) {
		$resstr .= "$cols[$i] - $row->[$i]\n";
	  }
	  $resstr .= "\n";
	  push(@results, $resstr);
	}
  }
  print "$#results result(s) found.\n";
  foreach my $i (1..$#results) {
	print "Result $i:\n";
	print "$results[$i-1]\n";
  }
}

while (1) {
  print "Enter search term: ";
  my $in = <STDIN>;
  chomp($in);
  if ($in eq 'q') { exit(0); }
  search($in);
}
